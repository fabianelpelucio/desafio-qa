# Define Product class
class Product:
    def __init__(self, item, unitPrice, specialAmount=None, specialPrice=None):
        self.item = item
        self.unitPrice = unitPrice
        self.specialAmount = specialAmount
        self.specialPrice = specialPrice

# Build products
A = Product("A", 50, 3, 130)
B = Product("B", 30, 2, 45)
C = Product("C", 20)
D = Product("D", 15)

# Dictionary
productDictionary = {'a':A, "b":B, "c":C, "d":D}

# Create Product List
def calculateTotalPrice(productList):
    aList = []
    bList = []
    cList = []
    dList = []
    for product in productList:
        if product.item == "A":
            aList.append(product);
        if product.item == "B":
            bList.append(product);
        if product.item == "C":
            cList.append(product);
        if product.item == "D":
            dList.append(product);

    totalA = calculateTotalItem(aList)
    totalB = calculateTotalItem(bList)
    totalC = calculateTotalItem(cList)
    totalD = calculateTotalItem(dList)

    return totalA + totalB + totalC + totalD

#calculate total for each item list
def calculateTotalItem(productList):
    productTemp = None
    if len(productList) == 0:
        return 0
    else:
        productTemp = productList[0]

        if  productTemp.specialPrice != None:
            totalSpecialAmount = len(productList) / productTemp.specialAmount
            totalSpecialPrice = int(totalSpecialAmount) * productTemp.specialPrice

            totalRestAmount = len(productList) % productTemp.specialAmount
            totalRestPrice = totalRestAmount * productTemp.unitPrice
            return totalSpecialPrice + totalRestPrice
        else:
            return len(productList) * productTemp.unitPrice

# Shopping cart
shoppingCartList = []

def shop():
    inputProduct = raw_input("Informe o item que deseja comprar (a, b, c, d):")
    if inputProduct.lower() in productDictionary:
        product = productDictionary[inputProduct.lower()]
        shoppingCartList.append(product)
        answerCart = raw_input("Deseja incluir um novo item ? (S/N):").lower()
        if answerCart == 's':
            shop()
        elif answerCart == 'n':
            print(calculateTotalPrice(shoppingCartList))
        else:
            print("Valor inválido. Por favor, tente novamente.")
            shop()
    else:
        print("Produto inválido. Por favor, tente novamente.")
        shop()

shop()
